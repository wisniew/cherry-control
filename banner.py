"""
  * Copyright (C) 2019 Rafael Wisniewski (wisniew99@gmail.com)
  * This file is part of Cherry Control (kernel control written in Python)
  * License: MIT
"""


import curses

def banner():
    stdscr = curses.initscr()
    stdscr.clear()
    stdscr.addstr(1, 3, "  .oooooo.     .oooooo. ")
    stdscr.addstr(2, 3, " d8P    Y8b   d8P    Y8b")
    stdscr.addstr(3, 3, "888          888        ")
    stdscr.addstr(4, 3, "888          888        ")
    stdscr.addstr(5, 3, "888          888        ")
    stdscr.addstr(6, 3, "`88b    ooo  `88b    ooo")
    stdscr.addstr(7, 3, " `Y8bood8P'   `Y8bood8P'")
    curses.endwin()
