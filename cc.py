#!/usr/bin/env python3
# coding: utf-8

"""
  * Copyright (C) 2019 Rafael Wisniewski (wisniew99@gmail.com)
  * This file is part of Cherry Control (kernel control written in Python)
  * License: MIT
"""

import curses
import getpass
from banner import banner
from about import about
from updates import updates

version = "0.1.1"

def main_menu():
    stdscr = curses.initscr()

    banner()
    curses.curs_set(0)
    stdscr.addstr( 10, 3, "1. Info (WIP)")
    stdscr.addstr( 11, 3, "2. Monitor (WIP)")
    stdscr.addstr( 12, 3, "3. Edit (WIP)")
    stdscr.addstr( 13, 3, "4. Other (WIP)")
    stdscr.addstr( 14, 3, "5. Profiles (WIP)")
    stdscr.addstr( 15, 3, "6. Updates")
    stdscr.addstr( 16, 3, "7. Settings (WIP)")
    stdscr.addstr( 17, 3, "8. About")
    stdscr.addstr( 18, 3, "0. Exit")

    input = stdscr.getch()

    if input == ord('1'):
        print("Work In Progress...")
    elif input == ord('2'):
        print("Work In Progress...")
    elif input == ord('3'):
        print("Work In Progress...")
    elif input == ord('4'):
        print("Work In Progress...")
    elif input == ord('5'):
        print("Work In Progress...")
    elif input == ord('6'):
        updates(version)
    elif input == ord('7'):
        print("Work In Progress...")
    elif input == ord('8'):
        about()
    elif input == ord('0'):
        curses.endwin()
        exit()
    else:
        stdscr.clear()
        stdscr.addstr( 1, 3, "ERROR!")
        stdscr.addstr( 2, 3, "You have entered an invalid selection!")
        stdscr.addstr( 3, 3, "Press any key to return to the main menu.")
        input = stdscr.getch()
        main_menu()
    main_menu()

username = getpass.getuser()
if username != "root":
    print("You need ROOT premissions to run CC!")
    exit(0)

main_menu()
