"""
  * Copyright (C) 2019 Rafael Wisniewski (wisniew99@gmail.com)
  * This file is part of Cherry Control (kernel control written in Python)
  * License: MIT
"""

import curses
import urllib.request
import os
import subprocess
import sys
from banner import banner

def updates(version):
    banner()
    stdscr = curses.initscr()
    exists = os.path.isfile('version_number.txt')

    stdscr.addstr( 10, 3, "Installed version: " + version)
    if exists:
        with open('version_number.txt', 'r') as file:
            version_new = file.read().replace('\n', '')
        stdscr.addstr( 11, 3, "Current version: " + version_new)
        stdscr.addstr( 13, 3, "1. Check for updates")
    else:
        stdscr.addstr( 12, 3, "1. Check for updates")
    if exists:
        if version_new > version:
            stdscr.addstr( 14, 3, "2. Install updates")
            stdscr.addstr( 16, 3, "0. Return.")
        else:
            stdscr.addstr( 15, 3, "0. Return.")
        
    else:
        stdscr.addstr( 14, 3, "0. Return.")

    curses.noecho()
    input = stdscr.getch()

    if input == ord('1'):
        curses.endwin()
        print("\033[H\033[J")
        print("\nDownloading latest version number...\n")
        urllib.request.urlretrieve("https://gitlab.com/wisniew/cherry-control/raw/PyCC/version_number.txt?inline=false", "version_number.txt")
        updates(version)
    elif input == ord('2'):
        curses.endwin()
        print("\033[H\033[J")
        print("\nDownloading latest version... ")
        urllib.request.urlretrieve("https://gitlab.com/wisniew/cherry-control/raw/PyCC/cc.bin?inline=false", "cc")
        print("\nConfigurating... ")
        subprocess.run(["chmod", "755", "cc"])
        print("\nRestarting... ")
        p = subprocess.Popen(["./cc"])
        p.wait()
        exit()
    elif input == ord('0'):
        pass
    else:
        stdscr.clear()
        stdscr.addstr( 1, 3, "ERROR!")
        stdscr.addstr( 2, 3, "You have entered an invalid selection!")
        stdscr.addstr( 3, 3, "Press any key to return to the main menu.")
        input = stdscr.getch()

    curses.endwin()