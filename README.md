# Cherry Control

**Cherry Control** *(PyCC)* is rewritten **Cherry Bash Commander** in Python 3 licenced under MIT. <BR>
<BR>
<BR>
**Branches:**<BR>
* **CC++** - Cherry Control written in C++ ***(no more maintained)***
* **PyCC** - Cherry Control written in Python 3 ***(no more maintained)***

**Features:**<BR>
* Updates system *(cc only for now)*

**Goals:**
* [ ] Grab all features from CBC
* [ ] Write fulll documantation
* [ ] Cherry deamon

**To do:**<BR>
* Info tab *(same as CBC)*
* Edit tab *(same as CBC)*
* Monitor tab *(same as CBC)*
* Others tab *(same as CBC)*
* settings tab *(same as CBC)*
* Update system for Python
* Cherry deamon
* Night mode for KCAL *(Cherry deamon)*
* remake menu *(Somthing like CC++)*

**Compile:**<BR>
* install nuitka: **sudo pip3 install nuitka**
* Compile to C++ using: **nuitka3 --follow-imports cc.py**
* New cc.bin will be in few seconds

**Requirements:**
* ROOT access *(Magisk recommended)*
* Recovery *(TWRP recommended)*
* Python 3 installed *(automatic installation with cc-installer.zip)*

**Installation:**<BR>
* Install [Termux terminal emulator](https://termux.com/)
* Run Termux and wait until installation finish
* Download latest cc-installer.zip
* Reboot to TWRP
* Flash cc-installer.zip
* Reboot to system

**Run:**<BR>
* Open termux
* type: **su** to grant root permissins
* type **./cc**

