"""
  * Copyright (C) 2019 Rafael Wisniewski (wisniew99@gmail.com)
  * This file is part of Cherry Control (kernel control written in Python)
  * License: MIT
"""

import curses
from banner import banner

def about():
    banner()
    stdscr = curses.initscr()
    stdscr.addstr( 10, 3, "Cherry Control is licenced under MIT.")
    stdscr.addstr( 11, 3, "Read LICENSE.md file for more info.")
    stdscr.addstr( 12, 3, "CC program was created by Wisniew in Python3 language.")
    stdscr.addstr( 13, 3, "CC helps configure and control Cherry kernel.")
    stdscr.addstr( 14, 3, "Please configure Cherry kernel with this tool only.")
    stdscr.addstr( 15, 3, "Issues/features requests are checked at gitlab only.")
    stdscr.addstr( 16, 3, "Thanks for using Cherry projects!")
    stdscr.addstr( 18, 3, "Press any key to return.")
    input = stdscr.getch()
    curses.endwin()